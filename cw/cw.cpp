﻿#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
    Mat square1(160, 160, CV_8UC1);
    square1 = 0;
    circle(square1, Point(square1.rows / 2, square1.cols / 2), 40, 127, -1, 8, 0);

    Mat square2(160, 160, CV_8UC1);
    square2 = 127;
    circle(square2, Point(square2.rows / 2, square2.cols / 2), 40, 0, -1, 8, 0);

    Mat square3(160, 160, CV_8UC1);
    square3 = 0;
    circle(square3, Point(square3.rows / 2, square3.cols / 2), 40, 255, -1, 8, 0);

    Mat square4(160, 160, CV_8UC1);
    square4 = 255;
    circle(square4, Point(square4.rows / 2, square4.cols / 2), 40, 0, -1, 8, 0);

    Mat square5(160, 160, CV_8UC1);
    square5 = 127;
    circle(square5, Point(square5.rows / 2, square5.cols / 2), 40, 255, -1, 8, 0);

    Mat square6(160, 160, CV_8UC1);
    square6 = 255;
    circle(square6, Point(square6.rows / 2, square6.cols / 2), 40, 127, -1, 8, 0);

    Mat image1, image2, image3;
    Mat image4(Mat::zeros(320, 480, CV_8UC1));
    hconcat(square1, square2, image1);
    hconcat(image1, square3, image1);

    hconcat(square4, square5, image2);
    hconcat(image2, square6, image2);

    vconcat(image1, image2, image1);

    imshow("result1", image1);

    Mat kernel1(2, 2, CV_32F);
    kernel1.at<float>(0, 0) = 1;
    kernel1.at<float>(1, 0) = 0;
    kernel1.at<float>(0, 1) = 0;
    kernel1.at<float>(1, 1) = -1;

    Mat kernel2(2, 2, CV_32F);
    kernel2.at<float>(0, 0) = 0;
    kernel2.at<float>(1, 0) = 1;
    kernel2.at<float>(0, 1) = -1;
    kernel2.at<float>(1, 1) = 0;

    //фильтр1
    filter2D(image1, image2, -1, kernel1, Point(-1, -1), 127);
    imshow("result2", image2);

    //фильтр2
    filter2D(image1, image3, -1, kernel2, Point(-1, -1), 127);
    imshow("result3", image3);

    for (int i = 0; i < image4.rows; i += 1)
        for (int j = 0; j < image4.cols; j += 1)
        {
            image4.at<uchar>(i, j) = floor(pow(pow(image2.at<uchar>(i, j), 2) + pow(image3.at<uchar>(i, j), 2), 0.5) / pow(2, 0.5));
        }
    imshow("result4", image4);

    waitKey(0);
}







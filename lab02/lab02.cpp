﻿#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

int main()
{
	//part1
	Mat image1;
	//загружаем картинку в png
	image1 = imread("C:/Users/lada.isakova/Desktop/isakova_l_m/projects/jpeg9565/bob1.png");
	std::vector<int> compression_params;
	compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
	compression_params.push_back(1);
	//уровень качества-95
	compression_params[1] = 95;
	//сохраняем картинку в jpeg-95
	imwrite("C:/Users/lada.isakova/Desktop/isakova_l_m/projects/jpeg9565/bob2.jpeg", image1, compression_params);
	//уровень качества-65
	compression_params[1] = 65;
	//сохраняем картинку в jpeg-65
	imwrite("C:/Users/lada.isakova/Desktop/isakova_l_m/projects/jpeg9565/bob3.jpeg", image1, compression_params);
	//imshow("", image1);
	Mat image2;
	image2 = imread("C:/Users/lada.isakova/Desktop/isakova_l_m/projects/jpeg9565/bob2.jpeg");
	//imshow("", image2);
	Mat image3;
	image3 = imread("C:/Users/lada.isakova/Desktop/isakova_l_m/projects/jpeg9565/bob3.jpeg");
	//imshow("", image3);
	//part2
	//общая картинка
	Mat diff_image2_b(Mat::zeros(164, 200, CV_8UC3));
	Mat diff_image2_g(Mat::zeros(164, 200, CV_8UC3));
	Mat diff_image2_r(Mat::zeros(164, 200, CV_8UC3));
	Mat diff_image3_b(Mat::zeros(164, 200, CV_8UC3));
	Mat diff_image3_g(Mat::zeros(164, 200, CV_8UC3));
	Mat diff_image3_r(Mat::zeros(164, 200, CV_8UC3));

	Mat diffimage(Mat::zeros(656, 600, CV_8UC3));
	for (int i = 0; i < 164; i += 1)
		for (int j = 0; j < 200; j += 1)
		{
			//значения bgr 1 картинки
			int b1 = image1.at<cv::Vec3b>(i, j)[0];
			int g1 = image1.at<cv::Vec3b>(i, j)[1];
			int r1 = image1.at<cv::Vec3b>(i, j)[2];
			//значения bgr 2 картинки
			int b2 = image2.at<cv::Vec3b>(i, j)[0];
			int g2 = image2.at<cv::Vec3b>(i, j)[1];
			int r2 = image2.at<cv::Vec3b>(i, j)[2];
			//значения bgr 3 картинки
			int b3 = image3.at<cv::Vec3b>(i, j)[0];
			int g3 = image3.at<cv::Vec3b>(i, j)[1];
			int r3 = image3.at<cv::Vec3b>(i, j)[2];

			//1 2 b
			diff_image2_b.at<cv::Vec3b>(i, j)[0] = 25 * abs(b1 - b2);
			diff_image2_b.at<cv::Vec3b>(i, j)[1] = 0;
			diff_image2_b.at<cv::Vec3b>(i, j)[2] = 0;
			//1 2 g
			diff_image2_g.at<cv::Vec3b>(i, j)[0] = 0;
			diff_image2_g.at<cv::Vec3b>(i, j)[1] = 25 * abs(g1 - g2);
			diff_image2_g.at<cv::Vec3b>(i, j)[2] = 0;
			//1 2 r
			diff_image2_r.at<cv::Vec3b>(i, j)[0] = 0;
			diff_image2_r.at<cv::Vec3b>(i, j)[1] = 0;
			diff_image2_r.at<cv::Vec3b>(i, j)[2] = 25 * abs(r1 - r2);

			//1 3 b
			diff_image3_b.at<cv::Vec3b>(i, j)[0] = 25 * abs(b1 - b3);
			diff_image3_b.at<cv::Vec3b>(i, j)[1] = 0;
			diff_image3_b.at<cv::Vec3b>(i, j)[2] = 0;
			//1 3 g
			diff_image3_g.at<cv::Vec3b>(i, j)[0] = 0;
			diff_image3_g.at<cv::Vec3b>(i, j)[1] = 25 * abs(g1 - g3);
			diff_image3_g.at<cv::Vec3b>(i, j)[2] = 0;
			//1 3 r
			diff_image3_r.at<cv::Vec3b>(i, j)[0] = 0;
			diff_image3_r.at<cv::Vec3b>(i, j)[1] = 0;
			diff_image3_r.at<cv::Vec3b>(i, j)[2] = 25 * abs(r1 - r3);
		}

	Mat image1_gray, image2_gray, image3_gray;
	cvtColor(image1, image1_gray, cv::COLOR_BGR2GRAY);
	cvtColor(image2, image2_gray, cv::COLOR_BGR2GRAY);
	cvtColor(image3, image3_gray, cv::COLOR_BGR2GRAY);
	Mat image95_gray = (image1_gray - image2_gray) * 25;
	Mat image65_gray = (image1_gray - image3_gray) * 25;

	Mat result;
	Mat result_image; 
	Mat result_bgr_95;
	Mat result_bgr_65;
	Mat result_gray;
	hconcat(image1, image2, result_image);
	hconcat(result_image, image3, result_image);
	hconcat(diff_image2_b, diff_image2_g, result_bgr_95);
	hconcat(result_bgr_95, diff_image2_r, result_bgr_95);
	hconcat(diff_image3_b, diff_image3_g, result_bgr_65);
	hconcat(result_bgr_65, diff_image3_r, result_bgr_65);
	hconcat(image1_gray, image95_gray, result_gray);
	hconcat(result_gray, image65_gray, result_gray);
	vconcat(result_image, result_bgr_95, result_image);
	vconcat(result_image, result_bgr_65, result_image);
	cvtColor(result_gray, result_gray, COLOR_GRAY2RGB);
	vconcat(result_image, result_gray, result_image);
	
	imshow("", result_image);

	waitKey(0);
	return 0;
}
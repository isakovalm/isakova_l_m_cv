﻿#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main()
{
    //part1
    Mat image1 = imread("C:/Users/lada.isakova/Desktop/isakova_l_m/projects/lab03/apple.png");
    cvtColor(image1, image1, COLOR_BGR2GRAY);
    imshow("image1", image1);

    //part2
    //массив для хранения гистограммы яркости
    int hist1[256];
    for (int i = 0; i <= 255; i += 1)
        hist1[i] = 0;
    //подсчитываем число пикселей картинки по каждому значению яркости от 0 до 255
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist1[image1.at<uchar>(i, j)] += 1;
        }      
    //максимальное число пикселей с одинаковой яркостью
    int max_count_pixel = 0;        
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist1[i] > max_count_pixel)
            max_count_pixel = hist1[i];
    }
    //картинка для отрисовки гистограммы
    Mat image2 = Mat(max_count_pixel, 256, CV_8UC3, Scalar(255, 255, 255));
    for (int i = 1; i <= 255; i++)
    {
        line(image2, Point(i - 1, hist1[i - 1]), Point(i, hist1[i]), 255, 1, 1, 0);
    }
    imshow("hist1", image2);

    //part3
    //окно для вывода графика преобразования яркости картинки
    Mat graph1 = Mat(256, 256, CV_8UC3, Scalar(255, 255, 255));
    //массив для хранения значений функций
    array<uchar, 256> val;
    //считаем значения функции для соответствующих яркостей
    for (int i = 0; i <= 255; i += 1)
    {
        if (i <= 64)
        {
            val.at(i) = i;
        }
        else
        {
            val.at(i) = abs(i - 2 * abs(i - 64));
        }
    }
    for (int i = 1; i <= 255; i++) //рисуем график функции
    {
        line(graph1, Point(i - 1, val[i - 1]), Point(i, val[i]), 255, 1, 1, 0);
    }
    imshow("graph1", graph1); //вывод графика на экран

    //part4
    Mat image3;
    LUT(image1, val, image3);
    //массив для хранения гистограммы яркости
    int hist2[256];
    for (int i = 0; i <= 255; i += 1)
        hist2[i] = 0;
    //подсчитываем число пикселей картинки по каждому значению яркости от 0 до 255
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist2[image3.at<uchar>(i, j)] += 1;
        }
    //максимальное число пикселей с одинаковой яркостью
    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist2[i] > max_count_pixel)
            max_count_pixel = hist2[i];
    }
    //картинка для отрисовки гистограммы
    Mat image4 = Mat(max_count_pixel, 256, CV_8UC3, Scalar(255, 255, 255));
    for (int i = 1; i <= 255; i++)
    {
        line(image4, Point(i - 1, hist2[i - 1]), Point(i, hist2[i]), 255, 1, 1, 0);
    }
    imshow("image2", image3);
    imshow("hist2", image4);

    //part5
    Mat clahe_image;
    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(3);
    clahe->setTilesGridSize(Size(10, 10));
    clahe->apply(image1, clahe_image);
    //массив для хранения гистограммы яркости
    int hist3[256];
    for (int i = 0; i <= 255; i += 1)
        hist3[i] = 0;
    //подсчитываем число пикселей картинки по каждому значению яркости от 0 до 255
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist3[clahe_image.at<uchar>(i, j)] += 1;
        }
    //максимальное число пикселей с одинаковой яркостью
    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist3[i] > max_count_pixel)
            max_count_pixel = hist3[i];
    }
    //картинка для отрисовки гистограммы
    Mat clahe_hist = Mat(max_count_pixel, 256, CV_8UC3, Scalar(255, 255, 255));
    for (int i = 1; i <= 255; i++)
    {
        line(clahe_hist, Point(i - 1, hist3[i - 1]), Point(i, hist3[i]), 255, 1, 1, 0);
    }
    imshow("clahe1", clahe_image);
    imshow("hist_clahe1", clahe_hist);

    clahe->setClipLimit(10);
    clahe->setTilesGridSize(Size(10, 10));
    clahe->apply(image1, clahe_image);
    //массив для хранения гистограммы яркости
    for (int i = 0; i <= 255; i += 1)
        hist3[i] = 0;
    //подсчитываем число пикселей картинки по каждому значению яркости от 0 до 255
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist3[clahe_image.at<uchar>(i, j)] += 1;
        }
    //максимальное число пикселей с одинаковой яркостью
    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist3[i] > max_count_pixel)
            max_count_pixel = hist3[i];
    }
    //картинка для отрисовки гистограммы
    for (int i = 1; i <= 255; i++)
    {
        line(clahe_hist, Point(i - 1, hist3[i - 1]), Point(i, hist3[i]), 255, 1, 1, 0);
    }
    imshow("clahe2", clahe_image);
    imshow("hist_clahe2", clahe_hist);

    clahe->setClipLimit(0.5);
    clahe->setTilesGridSize(Size(15, 15));
    clahe->apply(image1, clahe_image);
    //массив для хранения гистограммы яркости
    for (int i = 0; i <= 255; i += 1)
        hist3[i] = 0;
    //подсчитываем число пикселей картинки по каждому значению яркости от 0 до 255
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist3[clahe_image.at<uchar>(i, j)] += 1;
        }
    //максимальное число пикселей с одинаковой яркостью
    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist3[i] > max_count_pixel)
            max_count_pixel = hist3[i];
    }
    //картинка для отрисовки гистограммы
    for (int i = 1; i <= 255; i++)
    {
        line(clahe_hist, Point(i - 1, hist3[i - 1]), Point(i, hist3[i]), 255, 1, 1, 0);
    }
    imshow("clahe3", clahe_image);
    imshow("hist_clahe3", clahe_hist);

    //part6 Метод Оцу
    //делаем картинку одноканальной - CV_8UC1
    Mat bin_image(256, 256, CV_8UC1);

    int m = 0; //сумма высот всех бинов, домноженных на положение их середины
    int n = 0; //сумма высот всех бинов

    int threshold = 0; //порог

    int alpha1 = 0; //сумма высот всех бинов для класса 1
    int beta1 = 0; //сумма высот всех бинов для класса 1, домноженных на положение их середины

    float dispersion = 0; //значение межклассовой дисперсии
    float max_dispersion = 0; //максимальное значение межклассовой дисперсии

    float w1 = 0, w2 = 0; //вероятности принадлежности к классам 1 и 2
    float a = 0; // a = a1 - a2, где a1, a2 - средние арифметические для классов 1 и 2

    //считаем высоты
    for (int t = 0; t <= 255; t += 1)
    {
        m += t * hist1[t];
        n += hist1[t];
    }

    for (int t = 0; t <= 255; t += 1)
    {
        //вычисляем суммы высот бинов для 1 класса
        alpha1 += t * hist1[t];
        beta1 += hist1[t];
        //находим вероятность принадлежности к 1 классу
        w1 = (float)beta1 / n;

        //находим разность средних арифметических
        a = (float)alpha1 / beta1 - (float)(m - alpha1) / (n - beta1);

        //вычисляем дисперсию
        dispersion = w1 * (1 - w1) * a * a;

        //находим максимальную межклассовую дисперсию и соответствующий ей порог
        if (dispersion > max_dispersion)
        {
            max_dispersion = dispersion;
            threshold = t;
        }
    }
    //применяем бинаризацию
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            if (image1.at<uchar>(i, j) < threshold)
                bin_image.at<uchar>(i, j) = 0;
            else
                bin_image.at<uchar>(i, j) = 255;
        }

    Mat res_image;
    hconcat(image1, bin_image, res_image);
    imshow("bin_image", res_image);

    //part7 Адаптивная бинаризация 
    Mat bin_adaptive_image;
    adaptiveThreshold(image1, bin_adaptive_image, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 29, 9);
    Mat res_adaptive_image;
    hconcat(image1, bin_adaptive_image, res_adaptive_image);
    imshow("bin_adaptive_image", res_adaptive_image);

    //part8 Морфологические фильтры
    Mat filter_image;
    Mat res_filter_image;
    morphologyEx(bin_image, filter_image, MORPH_OPEN, Mat(), Point(-1, 1), 1, 0);
    hconcat(bin_image, filter_image, res_filter_image);
    imshow("filter_image", res_filter_image);

    //part9 Бинарная маска
    Mat alpha_image;
    double alpha = 0.7;
    addWeighted(image1, alpha, filter_image, 1 - alpha, 0.0, alpha_image);
    imshow("alpha_image", alpha_image);

    waitKey(0);
    return 0;
}

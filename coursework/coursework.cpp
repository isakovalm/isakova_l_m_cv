﻿#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>


using namespace cv;
using namespace std;

Mat ReadImage(string path)
{
    Mat image;
    image = imread(path, IMREAD_REDUCED_COLOR_4);
    return image;
}

int* StartPoints(string path_point)
{
    int* points = new int[8];
    int n = 0;
    int a;
    fstream F;

    //открываем файл в режиме чтения
    F.open(path_point);
    //если открытие файла прошло корректно, то
    if (F)
    {
        //цикл для чтения значений из файла; выполнение цикла прервется,
        //когда достигнем конца файла, в этом случае F.eof() вернет истину.
        while (!F.eof())
        {
            //чтение очередного значения из потока F в переменную a
            F >> a;
            points[n] = a / 4;
            n += 1;
        }
        //закрытие потока
        F.close();
    }

    return points;
}

Mat LineImage(int* points, Mat image)
{
    Mat line_image;
    line_image = image.clone();
    for (int i = 0; i < 6; i += 2)
    {
        line(line_image, Point(points[i], points[i + 1]), Point(points[i + 2], points[i + 3]), Scalar(0, 0, 255), 2);
    }
    line(line_image, Point(points[0], points[1]), Point(points[6], points[7]), Scalar(0, 0, 255), 2);

    return line_image;
}

int* FinishPoints(int* points)
{
    int* new_points = new int[8];
    int xmin, xmax, ymin, ymax;
    xmin = min(points[0], points[6]);
    xmax = max(points[2], points[4]);
    ymax = max(points[5], points[7]);
    ymin = min(points[1], points[3]);
    new_points[0] = xmin; new_points[1] = ymin;
    new_points[2] = xmax; new_points[3] = ymin;
    new_points[4] = xmax; new_points[5] = ymax;
    new_points[6] = xmin; new_points[7] = ymax;
     
    return new_points;
}

Mat NewLineImage(int* new_points, Mat line_image)
{
    Mat new_line_image;
    new_line_image = line_image.clone();
    for (int i = 0; i < 6; i += 2)
    {
        line(new_line_image, Point(new_points[i], new_points[i + 1]), Point(new_points[i + 2], new_points[i + 3]), Scalar(0, 255, 0), 2);
    }
    line(new_line_image, Point(new_points[0], new_points[1]), Point(new_points[6], new_points[7]), Scalar(0, 255, 0), 2);

    return new_line_image;
}

double* QualityControl(int* points, int* new_points)
{
    double* distance_points = new double[4];
    for (int i = 0; i <= 3; i += 1)
    {
        distance_points[i] = sqrt(pow((points[2 * i] - new_points[2 * i]), 2) + pow((points[2 * i + 1] - new_points[2 * i + 1]), 2));
    }
    double min_distance, max_distance;
    min_distance = min(distance_points[0], distance_points[1]);
    min_distance = min(min_distance, distance_points[2]);
    min_distance = min(min_distance, distance_points[3]);
    max_distance = max(distance_points[0], distance_points[1]);
    max_distance = max(min_distance, distance_points[2]);
    max_distance = max(min_distance, distance_points[3]);
    double* distance_result = new double[2];
    distance_result[0] = min_distance;
    distance_result[1] = max_distance;
    return distance_result;
}

Point2f* QuadPoints(int* points)
{

    // Получаем координаты четырёхугольника
    Point2f* input_quad = new Point2f[4];

    for (int i = 0; i <= 3; i++)
    {
        input_quad[i] = Point2f(points[2 * i], points[2 * i + 1]);
    }

    return input_quad;
}

int* ResultPoints(int* new_points)
{
    int* result_points = new int[8];
    for (int i = 0; i <= 7; i += 2)
    {
        result_points[i] = new_points[i] - new_points[0];
        result_points[i + 1] = new_points[i + 1] - new_points[1];
    }

    return result_points;
}

int main()
{
    string path, path_point, path_line, path_new_line, path_perspective_transform, path_result, path_quality_control;

    for (int i = 0; i <= 69; i += 1)
    {
        //считываем исходное изображение
        path = "C:/Users/lada.isakova/Desktop/isakova_l_m/projects/coursework/dataset/image_" + to_string(i + 1) + ".png";
        Mat image;
        image = ReadImage(path);
        //считываем исходную разметку
        path_point = "C:/Users/lada.isakova/Desktop/isakova_l_m/projects/coursework/dataset_marking/image_" + to_string(i + 1) + ".txt";
        int* points = new int[8];
        points = StartPoints(path_point);
        //наносим разметку на исходное изображение
        path_line = "C:/Users/lada.isakova/Desktop/isakova_l_m/projects/coursework/dataset_marking/image_" + to_string(i + 1) + ".png";
        Mat line_image;
        line_image = LineImage(points, image);
        imwrite(path_line, line_image); //сохраняем изображение
        //пересчитываем разметку
        int* new_points = new int[8];
        new_points = FinishPoints(points);
        //наносим финальную разметку
        path_new_line = "C:/Users/lada.isakova/Desktop/isakova_l_m/projects/coursework/dataset_new_marking/image_" + to_string(i + 1) + ".png";
        Mat new_line_image;
        new_line_image = NewLineImage(new_points, line_image);
        imwrite(path_new_line, new_line_image); //сохраняем изображение
        //считаем минимум и максимум улета вершин от исходной разметки до финальной
        double* distance_result = new double[2];
        distance_result = QualityControl(points, new_points);
        cout << distance_result[1] << endl;
        //составляем матрицы вершин 4-угольников
        Point2f* input_quad = new Point2f[4];
        input_quad = QuadPoints(points);
        Point2f* output_quad = new Point2f[4];
        output_quad = QuadPoints(new_points);
        //выполняем перспективное преобразование
        Mat perspective_transform = Mat::zeros(image.rows, image.cols, image.type());
        perspective_transform = getPerspectiveTransform(input_quad, output_quad);
        Mat perspective_transform_image = Mat(image.rows, image.cols, image.type());
        warpPerspective(image, perspective_transform_image, perspective_transform, perspective_transform_image.size());
        path_perspective_transform = "C:/Users/lada.isakova/Desktop/isakova_l_m/projects/coursework/dataset_perspective_transform/image_" + to_string(i + 1) + ".png";
        imwrite(path_perspective_transform, perspective_transform_image); //сохраняем изображение
        //результирующая разметка, чтобы обрезать изображение
        int* result_points = new int[8];
        result_points = ResultPoints(new_points);

        Point2f* result_quad = new Point2f[4];
        result_quad = QuadPoints(result_points);
        //выполняем перспективное преобразование и получаем конечный результат
        Mat result_perspective_transform = Mat::zeros(image.rows, image.cols, image.type());
        result_perspective_transform = getPerspectiveTransform(input_quad, result_quad);
        Mat result_image = Mat(result_points[5], result_points[2], image.type());
        warpPerspective(image, result_image, result_perspective_transform, result_image.size());
        path_result = "C:/Users/lada.isakova/Desktop/isakova_l_m/projects/coursework/dataset_result/image_" + to_string(i + 1) + ".png";
        imwrite(path_result, result_image); //сохраняем преобразованное изображение
        //сохраняем разметки и параметры оценки качества
        path_quality_control = "C:/Users/lada.isakova/Desktop/isakova_l_m/projects/coursework/quality_control/image_" + to_string(i + 1) + ".txt";
        ofstream file(path_quality_control);

        if (file.is_open())
        {
            string marking1, marking2, marking3;
            for (int j = 0; j <= 7; j += 2)
            {
                marking1 += "[" + to_string(points[j]) + ", " + to_string(points[j + 1]) + "] ";
            }
            for (int j = 0; j <= 7; j += 2)
            {
                marking2 += "[" + to_string(new_points[j]) + ", " + to_string(new_points[j + 1]) + "] ";
            }
            for (int j = 0; j <= 7; j += 2)
            {
                marking3 += "[" + to_string(result_points[j]) + ", " + to_string(result_points[j + 1]) + "] ";
            }
            file << "Start marking" << endl;
            file << marking1 << endl;
            file << "Intermediate marking" << endl;
            file << marking2 << endl;
            file << "Result marking" << endl;
            file << marking3 << endl;
            file << "Min distance " + to_string(distance_result[0]) << endl;
            file << "Max distance " + to_string(distance_result[1]) << endl;
        }
        file.close();
    }

	waitKey(0);
	return 0;
}






